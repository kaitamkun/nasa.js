#!/usr/local/bin/node
const
	fs = require("fs"),
	path = require("path"),
	moment = require("moment"),
	httpreq = require("httpreq"),
	ansi = require("ansi.js"),
	Promise = require("bluebird"),
	async = require("async"),
	chrono = require("chrono-node"),
	filesize = require("filesize"),
	_touch = Promise.promisify(require("touch")),
	child_process = require("child_process"),
	shellescape = require("shell-escape"),
	death = require("death"),
	// inquirer = require("inquirer-bluebird"),
	inquirer = require("inquirer"),
	PathPrompt = require("inquirer-prompt-path").PathPrompt


const {
	red, orange, yellow, green, cyan, blue, magenta, black, gray, lightGray, white,
	bold, underline, reset, save, restore, print, check, nope, arrows, grays, pgray,
	show, hide, left, resetLine, warning
} = ansi;
const { yes, no, whoa, die, spinners, spinner, bar, B, C, Yes, No, Whoa } = ansi.etc;
death((signal, err) => print("%" + ansi.show));

// console.log(process.argv, process.cwd());

// inquirer.prompts["path"] = PathPrompt;
inquirer.prompt.registerPrompt("path", PathPrompt);
// const prompt = Promise.promisify(inquirer.prompt);
const NASAJS_BASE = path.join(process.argv[1], "../");
const NASAJS_CONFIG_PATH = path.join(NASAJS_BASE, "config/nasa.json");
let HYPERNASA_BASE, HYPERNASA_CONFIG_PATH;
// const base = path.join(fs.readFileSync("config/hypernasa_path", { encoding: "utf8" }).trim(), "/");
const args = process.argv.slice(2), verb = (args[0] || "").toLowerCase();
const exists = (f, types=["f"], absolute=false) => new Promise((resolve, reject) => fs.access(absolute? f : path.join(HYPERNASA_BASE, f), types.map(c => fs.constants[`${c.toUpperCase()}_OK`]).reduce((a, b) => a | b), (err) => resolve(!err)));
const [_read, _write, _unlink, _symlink, _readdir] = "readFile writeFile unlink symlink readdir".split(" ").map(s => Promise.promisify(fs[s]));
const read = (f, absolute=false) => _read(absolute? f : path.join(HYPERNASA_BASE, f), { encoding: "utf8" });
const write = (f, data, absolute=false) => _write(absolute? f : path.join(HYPERNASA_BASE, f), data);
const unlink = (f, absolute=false) => _unlink(absolute? f : path.join(HYPERNASA_BASE, f));
const info = (...x) => console.info(` ${bold + cyan}(${reset + bold}i${reset + bold + cyan})${reset}`, ...x);
const symlink = (source, destination, force=true, absolute=false) => force? new Promise((resolve, reject) => resolve(exists(destination, absolute).then(() => unlink(destination, absolute)).then(() => _symlink(base + source, base + destination)).catch(() => resolve(_symlink(absolute? source : path.join(HYPERNASA_BASE, source), absolute? destination : path.join(HYPERNASA_BASE, destination)).catch((err) => reject(err)))))) : _symlink(absolute? source : path.join(HYPERNASA_BASE, source), absolute? destination : path.join(HYPERNASA_BASE, destination));
const readdir = (f, fatal=true, absolute=false) => _readdir(absolute? f : path.join(HYPERNASA_BASE, f)).catch(fatal && ((err) => die(`${red}An error occurred while trying to read ${reset + bold + f + reset}:`, err)));
const plural = (word, count, suffix="s") => count == 1? word : word + suffix;
const touch = (f, absolute=false, options) => _touch(absolute? f : path.join(HYPERNASA_BASE, f), options);
const http = {
	get: (url, options={ }, ignoreStatus=false) => new Promise((resolve, reject) => httpreq.get(url, options, (err, res) => err? reject(err) : (ignoreStatus || res.statusCode == 200? resolve(res) : reject([res.statusCode, res]))))
};

// intercepts a rejection containing an array [error message, full error], freezes the full error and then throws the error message. yuck.
const trace = (e, filename="trace") => e instanceof Array? freeze({ error: e[1], string: e[1].toString(), inspection: e[1].inspect? e[1].inspect() : null }, path.join(NASAJS_BASE, "traces", filename), true).then(() => { throw e[0] }) : trace([e, e], filename);

const transformError = (err) => {
	let s = err.toString();
	if (s.match(/\bENOTFOUND\b/)) return ["No internet connection.", err];
	if (s.match(/\bETIMEDOUT\b/)) return ["Connection timed out.", err];
	if (s.match(/\bECONNRESET\b/)) return ["Connection reset.", err];
	return [s, err];
};

const thaw = (filename, absolute=false, fatal=true) => {
	return new Promise((resolve, reject) => {
		read(absolute? filename : path.join(HYPERNASA_BASE, filename), absolute).then((raw) => {
			try {
				resolve(JSON.parse(raw));
			} catch(e) {
				if (fatal) {
					die(`${red}An error occurred while trying to parse data${reset}:`, e);
				};

				reject(e);
			};
		}).catch((e) => {
			if (fatal)  {
				die(`${red}An error found while trying to thaw data from ${reset+bold+filename+reset}:\n`, e);
			};

			reject(e);
		});
	});
};

const freeze = (data, filename, absolute=false, silent=true, fatal=true) => {
	filename = absolute? filename : path.join(HYPERNASA_BASE, filename);
	silent || console.log(`Freezing to ${filename}:`, data);
	return write(filename, JSON.stringify(data), absolute).catch((e) => {
		if (fatal) {
			die(`${red}An error occurred while trying to freeze data to ${reset+bold+filename+reset}:`, e);
		};

		return Promise.reject(e);
	});
};

if (verb == "cd") { // try running "`nasa cd`" or "`nasa cd cache`"
	return thaw(NASAJS_CONFIG_PATH, true, false).then((config) => {
		if (!config.base) throw "no base";
		console.log(`cd ${config.base}`);
	}).catch((err) => console.log(`echo \\040${no} Couldn't read configuration.`));
};

const getID = (config, args, checkRange=true, fatal=true) => {
	let arg = args[0] || "";
	if (arg.length == 6) {
		arg = `${arg.substr(0, 2) < 96? "20" : "19"}${arg}`;
	};

	if (arg.match(/^\d{6,8}$/)) {
		return [arg, moment(new Date(arg.replace(/^(\d{2,4})(\d{2})(\d{2})$/, "$2/$3/$1"))), arg.replace(/.+(\d{6})$/, "$1")];
	};

	let date;
	if (arg.match(/^cur(rent)?$/i)) {
		if (config && config.hypernasa && config.hypernasa.selected) {
			date = new Date(config.hypernasa.selected);
		} else {
			return fatal? die(`${red}Error${reset}: No date seems to be selected.`) : null;
		};
	} else {
		date = arg == ""? new Date() : chrono.parseDate(args.join(""));
	};

	// moment's parsing kinda sucks :/, whereas Date will accommodate almost anything
	if (date === null || isNaN(date.valueOf())) {
		return fatal? die(`${red}Unable to parse ${reset + bold + args[0] + reset + red} as a date.${reset}`) : null;
	};

	const m = moment(date);
	if (m.isBefore("1995-06-16")) {
		die(` ${whoa} NASA's APOD archives don't extend past 1995/6/16.`);
	};

	return [m.format("YYYYMMDD"), m, m.format("YYMMDD")];
};

const run = (config, verb, ...args) => new Promise((resolve, reject) => {
	const showHelp = (text, force=false) => {
		if (force || (args[0] || "").match(/^(h(elp)?|\?+)$/)) {
			console.log(`Usage: ${bold}nasa ${verb}${(text? ` ${text}` : "") + reset}`);
			process.exit(0);
		};
	};

	if (verb.match(/^last(checked)?$/)) {
		showHelp();
		if (typeof config.hypernasa.lastChecked == "undefined") {
			console.log(`Last checked: ${yellow}unknown${reset}`);
		} else {
			console.log(`Last checked: ${bold+config.hypernasa.lastChecked+reset}`);
		};
	} else if (verb.match(/^(now|current)$/)) {
		const say = (s) => console.log(`Current choice: ${bold+s+reset}`);
		if (typeof config.hypernasa.selected == "undefined") {
			exists(`cache/image.jpg`).then((result) => say(result? (typeof config.hypernasa.lastChecked == "undefined"? `${yellow}unknown` : config.hypernasa.lastChecked) : `${red}none`));
		} else {
			say(config.hypernasa.selected);
		};
	} else if (verb.match(/^get$/)) {
		showHelp("[date]");
		const [id, date, short] = getID(config, args);

		if (!args[1]) {
			print(`${hide}Retrieving ${bold + short + reset}.`);
		};

		httpreq.get(`https://apod.nasa.gov/apod/ap${short}.html`, (err, res) => {
			print(resetLine);
			if (err) {
				return trace(transformError(err), `ap${short}_trace`).catch((e) => die(`${red}An error occurred while trying to retrieve NASA APOD data${reset}:`, transformError(e)[0]));
			};

			const match = res.body.match(/<a href="(image([^"]+))"/i);
			if (match == null) {
				die(`${red}Unable to parse NASA APOD data for ${reset + bold + short + reset + red}. There is probably no image for that date.`);
			};

			httpreq.download(`https://apod.nasa.gov/apod/${match[1]}`, path.join(HYPERNASA_BASE, "cache", `${id}.jpg`), (err, progress) => {
				if (err) {
					die(`${red}An error occurred while trying to download ${bold}https://apod.nasa.gov/apod/${match[1]+reset}:`, err);
				};

				print(`${resetLine+reset} ${spinner(progress.percentage, 8)} ${bar(progress.percentage / 100)} ${pgray(50 + progress.percentage * 0.5) + Math.round(progress.percentage)}%${reset}`);
			}, (err, res) => {
				console.log(`${show + resetLine} ${yes} ${bold + moment(date).format("MM/DD/YYYY") + reset} ${pgray(40) + bold + arrows.right + reset} ${bold + res.downloadlocation + reset}${res.headers["content-length"]? ` (${cyan+filesize(res.headers["content-length"], {standard: "iec"})+reset})`: ""}`);
				resolve(`cache/${id}.jpg`);
			});
		});

		return;
	} else if (verb == "use") {
		showHelp("<date>");
		const [id, date, short] = getID(config, args);
		const makeSymlink = () => unlink(`cache/image.jpg`).then(() => symlink(`cache/${id}.jpg`, `cache/image.jpg`)).catch((err) => die(`${red}An error occurred while trying to make a symlink to ${bold+id}.jpg${reset}:`, err));
		exists(`cache/${id}.jpg`).then((result) => {
			if (result) {
				makeSymlink().then(() => {
					console.log(` ${yes} Found ${bold + id + reset} in cache; created symlink.`);
					config.hypernasa.selected = date.format("YYYY/MM/DD");
					freeze(config.hypernasa, HYPERNASA_CONFIG_PATH, true).then(() => resolve());
				});
			} else {
				console.log(` ${whoa} Not found in cache. Retrieving from the NASA APOD archive.`);
				run(config, "get", id, true).then(makeSymlink).then(() => {
					console.log(` ${yes} Created symlink from ${bold}cache/${id}.jpg${reset} to ${bold}cache/image.jpg${reset}.`);
					config.hypernasa.selected = date.format("YYYY/MM/DD");
					freeze(config.hypernasa, HYPERNASA_CONFIG_PATH, true).then(() => resolve());
				});
			};
		});
	} else if (verb == "test") {
		const topic = args[0] || "";
		if (topic.match(/^p(romises?)?$/)) {
			return resolve(require("/Users/kai/src/promises_test.js").run());
		} else if (topic.match(/^unlink$/)) {
			resolve(new Promise((resolve, reject) => {
				const fakename = `${Math.random().toString().substr(2, 6)}.txt`;
				info(`Trying to unlink nonexistent file ${underline + fakename + reset}.`);
				resolve();
			}));
		} else if (topic.match(/^r(ead(dir)?)?$/)) {
			return readdir("cache").then((x) => console.log(`readdir results:\n${x.map(s => ` - ${bold+s+reset}`).join("\n")}`));
		} else if (topic.match(/^exists$/)) {
			exists(`.`).then((firstResult) => {
				console.log(`Does ${bold}exists()${reset} return true for the current directory?`, firstResult? `${yes} Yes` : `${no} No`);
				exists(Math.random().toString().substr(2)).then((secondResult) => {
					console.log(`Does ${bold}exists()${reset} return false for a non-existent file? `, !secondResult? `${yes} Yes` : `${no} No`);
					resolve();
				});
			});

			return;
		} else {
			showHelp("<topic>", true);
		};
	} else if (verb == "clear") {
		showHelp();
		let originals;
		return readdir("cache").then((files) => async.series((originals = files.filter((filename) => filename.match(/^(\d{6,8}|image)\.jpg$/))).map((filename) => (next) => unlink(`cache/${filename}`).then(next)), () => {
			let l = originals.length;
			console.log(l? ` ${yes} Removed ${bold+l+reset} file${l == 1? "" : "s"}.` : ` ${whoa} No files were removed; cache was already empty.`);
			resolve(originals)
		}));
	} else if (verb == "count") {
		showHelp();

		return readdir("cache").then((files) => {
			const count = files.filter((filename) => filename.match(/^\d+\.jpg$/)).length;
			const linked = files.indexOf("image.jpg") != -1;
			if (count == 0) {
				console.log(`The cache contains no files ${linked? "but one" : "and no"} symlink.`);
			} else {
				console.log(`The cache contains ${bold + count} ${plural("file", count) + reset} ${linked? "and a" : "but no"} symlink.`);
			};

			resolve(count);
		});
	} else if (verb == "cached") {
		return readdir("cache").then((files) => {
			console.log(`Cached:`, files.filter((filename) => filename.match(/^\d{6,8}\.jpg$/)).map((filename) => filename.replace(/^(\d{2,4})(..)(..)\.jpg$/, `${bold}$1${reset+pgray(50)}/${reset+bold}$2${reset+pgray(50)}/${reset+bold}$3${reset}`)).join(", "));
			resolve();
		});
	} else if (verb.match(/^o(pacity)?$/)) {
		// if (!config.hyperjs) {
			// die(` ${no} I don't know where your ${B(".hyper.js")} is located. Try ${B("nasa config set")}.`);
		// };

		let opacity = args[0]? parseFloat(args[0]) : 0.5, oldOpacity;
		if (isNaN(opacity)) {
			console.log(` ${whoa} Unable to parse ${B(args[0])} as a number.`);
			return resolve();
		};

		return read("/Users/kai/.hyper.js", true).then((text) => {
			if (!text.match(/overlayOpacity\s*:\s*\S+/)) {
				return console.log(` ${whoa} Couldn't find opacity value in ${B(".hyper.js")}.`);
			};

			oldOpacity = text.match(/overlayOpacity\s*:\s*(\S+)/)[1];
			if (!args[0] || opacity == null) {
				console.log(` ${yes} Current opacity: ${B(oldOpacity)}`);
				return true;
			};

			return write("/Users/kai/.hyper.js", text.replace(/(overlayOpacity\s*:\s*)\S+/, `$1${opacity}`), true);
		}).then((skip=false) => resolve(void(skip || console.log(` ${yes} Replaced old opacity ${B(oldOpacity)} with ${B(opacity)}.`))));
	} else if (verb == "blank") {
		exists("cache/image.jpg").then((result) => result? unlink("cache/image.jpg") : Promise.resolve()).then(() => touch("cache/image.jpg")).then(() => { console.log(` ${yes} Blanked ${bold}cache/image.jpg${reset}.`); resolve() }).catch((err) => die(`??? ${err}`));
	} else if (verb == "open") {
		if ((args[0] || "").match(/^i(m(age|g)?)?$/i)) {
			if (config.hypernasa.selected) {
				child_process.exec(`open ${path.join(HYPERNASA_BASE, "cache", moment(new Date(config.hypernasa.selected)).format("YYYYMMDD"))}.jpg`);
			} else {
				die(` ${no} Can't find current selection.`);
			};
		} else {
			child_process.exec(`open ${path.join(HYPERNASA_BASE, args[0] == "cache" || args[0] == "c"? "cache/" : "")}`);
		};
	} else if (verb.match(/^(info|details|(cap|explana)tion)$/)) {
		showHelp("[date]");
		let [id, date, short] = getID(config, args.length || typeof config.hypernasa.selected == "undefined"? args : [config.hypernasa.selected]);
		print(`${hide}Retrieving ${bold + short + reset}...`);
		return http.get(`https://apod.nasa.gov/apod/ap${short}.html`).then(({ body }) => {
			print(resetLine + show);
			const match = body.split(/\s*\n\s*/).join(" ").match(/Explanation: \<\/b\>(.+)<p>\s*<center>/);
			if (!match) {
				die(`${red}Couldn't find information from ${reset+bold}https://apod.nasa.gov/apod/ap${short}.html${reset}.`);
			};

			console.log(`${B(date.format("MMMM D, YYYY"))}:\n${match[1].replace(/<[^>]+>/g, "").replace(/ -+ /g, "—").trim()}`);
		}).catch((e) => { print(resetLine + show); return reject(["Couldn't fetch information.", e]) });
	} else if (verb == "browse") {
		showHelp("[date]");
		const [id, date, short] = getID(config, args);
		child_process.exec(`open "https://apod.nasa.gov/apod/ap${short}.html"`);
	} else if (verb == "") {
		console.log(`Usage: ${bold}nasa [command]${reset}`);
		console.log(`Commands:`, "last current get use test clear count cached blank open caption".split(" ").sort().map(s => `\n  - ${bold+s+reset}`).join(""));
	} else {
		console.log(`Unknown command ${bold+verb+reset}.`);
	};

	resolve();
});

thaw(NASAJS_CONFIG_PATH, true, false).catch((e) => {
	Whoa(`I can't read ${B(NASAJS_CONFIG_PATH)}. Let's create one.`);
	return inquirer.prompt([{ type: "path", name: "choice", default: process.cwd(), message: () => "Base directory for hypernasa:" }])
		.then(({ choice }) => {
			let config = { base: choice };
			return freeze(config, NASAJS_CONFIG_PATH, true).then(() => ({ choice, config }));
		}).then(({ choice, config }) => console.log(`${yes} Saved configuration to ${B(NASAJS_CONFIG_PATH)}.\n`) || config);
})
.then((config) => thaw(HYPERNASA_CONFIG_PATH = path.join(config.base, "config.json"), true).then((hyperconfig) => { config.hypernasa = hyperconfig; return config }))
.then((config) => (HYPERNASA_BASE = config.base)? run(config, verb, ...args.slice(1)) : die(` ${no} Couldn't read ${B("base")} from ${B("nasa.json")}. It may be corrupted or incomplete.`))
.catch(trace)
.catch((err) => die(`${red}Error${reset}:`, err));
